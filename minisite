#!/usr/bin/env ruby

## Code written by Lasse Kliemann <lasse@lassekliemann.de>
## in August 2020 and February 2021.
## See https://gitlab.com/lxkl/minisite for the latest version
## and for documentation.

$VERBOSE = true
require "fileutils"
require "pathname"
require "webrick"
require "yaml"

def compile_if_outdated(fn_list, target_ext, include_list)
  all_good = true
  fn_list.each do |fn|
    target_fn = Pathname.new(fn).sub_ext(target_ext)
    if (! File.exist?(target_fn)) || ([fn] + include_list).any? { |x| File.mtime(x) >= File.mtime(target_fn) }
      puts "minisite: compiling #{fn} into #{target_fn}..."
      yield fn, target_fn
      unless $?.success?
        all_good = false
        puts "minisite: error compiling #{fn} into #{target_fn}"
        FileUtils.rm_f(target_fn)
      end
    end
  end
  all_good
end

def exec_rerun
  ARGV.shift
  exec("rerun", "--pattern", "**/*.{adoc,scss,yaml}", __FILE__, *ARGV)
end

if (! ARGV.empty?) && (! ["rerun", "rerun-http", "upload"].include?(ARGV[0]))
  puts "usage: minisite [rerun|rerun-http|upload] ..."
  exit 1
end
exit 0 unless File.exist?("config.yaml")
config = YAML.load(File.read("config.yaml"))
exit 0 unless config
exec_rerun if ARGV[0] == "rerun"
if ARGV[0] == "rerun-http"
  fork do
    server = WEBrick::HTTPServer.new(:BindAddress => config["http_bind_address"] || "127.0.0.1",
                                     :Port => config["http_port"] || 8000,
                                     :DocumentRoot => File.expand_path("."))
    server.start
  end
  exec_rerun
end
["adoc", "scss", "include", "upload", "rsync_url"].each do |x|
  config[x] = [] unless config[x]
end
all_good = compile_if_outdated(config["adoc"], ".html", config["include"]) do |fn, target_fn|
  n = target_fn.cleanpath.each_filename.to_a.size - 1
  root = n.times.map { ".." }.join("/")
  root = "." if root.empty?
  system("asciidoctor", "--attribute=root_=#{root}", "--attribute=outfile_basename_=#{File.basename(target_fn)}", "--failure-level=ERROR", fn)
end
all_good = compile_if_outdated(config["scss"], ".css", config["include"]) do |fn, target_fn|
  system("scss", fn, out: target_fn.to_s)
end && all_good
unless all_good
  puts "minisite: there were errors, see above"
  exit 1
end
all_good = true
if ARGV[0] == "upload"
  config["rsync_url"].each do |url|
    puts "minisite: uploading to #{url}..."
    system("rsync", "-v", "-rlptD", "--delete", "--delete-excluded", "--chmod=a+rX",
           *(config["upload"].map { |x| "--include=/#{x}" }), "--exclude=*", "./", url)
    unless $?.success?
      puts "minisite: error uploading to #{url}"
      all_good = false
    end
  end
end
unless all_good
  puts "minisite: there were uploading errors, see above"
  exit 1
end
